import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { DoctorService } from '../services/doctor.service';
import { IDoctor } from '../Entity/doctor';
import { SessionService } from '../services/session.service';

@Component({
  selector: 'app-doctor-detail',
  templateUrl: './doctor-detail.component.html',
  styleUrls: ['./doctor-detail.component.css']
})
export class DoctorDetailComponent implements OnInit {
  doctorId;
  doctor: IDoctor;
  constructor(private route: ActivatedRoute, private session: SessionService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('id'));
      this.doctorId = id;
    });
    this.doctor = this.session.getDoctorSession();
  }

  
}
