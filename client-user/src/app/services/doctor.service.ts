import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { FormBuilder } from '@angular/forms';
import { IDoctor } from '../Entity/doctor';
import { IHospital } from '../Entity/hospital';
import { IDepartment } from '../Entity/department';
@Injectable({
  providedIn: 'root'
})
export class DoctorService {
  constructor(private _http: HttpClient, private fb: FormBuilder) { }
  // _url = '../assets/fixedData/record.json';
  //_url = 'http://localhost:3001/entity';
  _url = 'http://localhost:3001/doctor';
  _url2 = 'http://localhost:3001/department'
  getAllDoctors(): Observable<IDoctor[]>{
    return this._http.get<IDoctor[]>(this._url)
                    .pipe(catchError(this.errorHandler));
  }
  errorHandler(error: HttpErrorResponse){
    return Observable.throw(error.message || "Server Error");
  }
  // Get doc tor by hospital department or doctor name
  getDoctorByFilter(hospitalId: number,departmentId: number, doctorName: string): Observable<IDoctor[]>{
    return this._http.get<IDoctor[]>(this._url  + '/' + hospitalId + '&' + departmentId + '&' + doctorName)
                    .pipe(catchError(this.errorHandler));
  }
  // get list all hospital name
  getListHospital(): Observable<IHospital[]>{
    return this._http.get<IHospital[]>(this._url)
                    .pipe(catchError(this.errorHandler));
  }
  // get list all department
  getListDepartment():Observable<IDepartment[]>{
    return this._http.get<IDepartment[]>(this._url2)
                    .pipe(catchError(this.errorHandler));
  }

  // addDoctor(userData){
  //   return this._http.post<any>(this._url, userData)
  //                    .pipe(catchError(this.errorHandler));
  // }

  // deleteDoctor(id: number){
  //   return this._http.delete<any>(this._url + '/' + id);
  // }


  // updateDoctor(id,userData){
  //   return this._http.put<any>(this._url + '/' + id, userData);
  // }

}
