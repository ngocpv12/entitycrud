import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './general-components/header/header.component';
import { PageNotFoundComponent } from './general-components/page-not-found/page-not-found.component';
import { DoctorService } from './services/doctor.service';
import {NgxPaginationModule} from 'ngx-pagination';
import { DoctorDetailComponent } from './doctor-detail/doctor-detail.component';

@NgModule({ 
  declarations: [
    AppComponent,
    routingComponents,
    HeaderComponent,
    PageNotFoundComponent,
    DoctorDetailComponent,
  ],
  entryComponents:[],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxPaginationModule
  ],
  providers: [DoctorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
