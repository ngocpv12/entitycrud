export interface IDoctor {
    id: number,
    name: string,
    gender: number,
    title: string,
    specialist: string,
    unit: string
  }