export interface IDepartment {
    id: number,
    name: string,
    description: string,
    type: string
  }