import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './general-components/page-not-found/page-not-found.component';
import { DoctorComponent } from './doctor/doctor.component';
import { DoctorDetailComponent } from './doctor-detail/doctor-detail.component';


const routes: Routes = [
  { path:'', redirectTo: '/doctors', pathMatch:'full'},
  // { path:'', component: AppComponent},
  { path:'doctors', component: DoctorComponent },
  { path: 'doctors/:id', component: DoctorDetailComponent },
  { path:'**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [DoctorComponent,
                                  PageNotFoundComponent,
                                  DoctorDetailComponent];