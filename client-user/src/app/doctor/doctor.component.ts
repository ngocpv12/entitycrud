import { Component, OnInit, ViewChild } from '@angular/core';
import { DoctorService } from '../services/doctor.service';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { IDoctor } from '../Entity/doctor';
import { IHospital } from '../Entity/hospital';
import { IDepartment } from '../Entity/department';
import { Router } from '@angular/router';
import { SessionService } from '../services/session.service';


// DEMO DELETE AFTER
interface Food {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-doctor',
  templateUrl: './doctor.component.html',
  styleUrls: ['./doctor.component.css']
})
export class DoctorComponent implements OnInit {
  searchText;
  selectedHospital; // combobox hospital value
  selectedDepartment; // combobox department value
  // list doctor
  doctors: Array<IDoctor>;
  // list hospital
  hospitals: Array<IHospital>;
  //list Department
  departments: Array<IDepartment>;
  totalDoctors: number;
  pageIndex: number=1;
  constructor( private _doctorService: DoctorService, private router: Router, private session: SessionService) {
    
  }
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  ngOnInit(): void {
    this.loadDoctorInfo();
    this.loadDepartmentInfo();
  }

  loadDoctorInfo() {
    console.log("I am loading DoctorInfo");
    this._doctorService.getAllDoctors()
      .subscribe(response => {
        this.doctors = response;
        this.totalDoctors = response.length;
      });
  }
  // load all hospital
  loadHospitalInfo(){
    console.log("I am loading hospitalInfo");
    this._doctorService.getListHospital()
      .subscribe(response => {
        this.hospitals = response;
      });
  }

  // load all department
  loadDepartmentInfo(){
    console.log("I am load ding department info");
    this._doctorService.getListDepartment()
      .subscribe(response => {
        this.departments = response;
      });
  }


  // function search doctor
  searchDoctor(hospitalId:number = null, departmentId:number = null,doctorName:string = null ){
    console.log("Executing search doctor");
    // CONTINUE HERE
    this._doctorService.getDoctorByFilter(hospitalId,departmentId,doctorName)
      .subscribe(response => {
        this.doctors = response;
      });
    
  }
  // function on select each card
  onSelect(doctor){
    this.router.navigate(['/doctors', doctor.id, {state: {data: doctor}}]);
    this.session.saveDoctorSession(doctor);
  }



  // temporary data
  foods: Food[] = [
    {value: '1', viewValue: 'Bệnh viện Bạch Mai'},
    {value: '2', viewValue: 'Bệnh viện Việt Đức'},
    {value: '3', viewValue: 'Bệnh Viện Hồng Ngọc'}
  ];

}
