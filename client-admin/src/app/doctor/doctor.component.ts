import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DoctorService } from '../services/doctor.service';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { IDoctor } from '../Entity/doctor';
import { DialogDoctorComponent } from '../dialog-doctor/dialog-doctor.component';


@Component({
  selector: 'app-doctor',
  templateUrl: './doctor.component.html',
  styleUrls: ['./doctor.component.css']
})
export class DoctorComponent implements OnInit {
  dataSource;
  public errorMsg;
  constructor(private dialog: MatDialog, private _doctorService: DoctorService) {
    
  }
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  ngOnInit(): void {
    this.loadDataTable();
  }
  displayedColumns: string[] = ['id', 'name', 'gender', 'title', 'specialist', 'unit', 'actions'];

  loadDataTable() {
    console.log("I am loading dataTable");
    this._doctorService.getAllDoctors()
      .subscribe(response => {
        this.dataSource = new MatTableDataSource(response);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  // logData(row){
  //   console.log(row);
  // }

  openDialog() {
    // let dialogConfig = new MatDialogConfig();
    // dialogConfig.width = "50%";
    let dialogRef = this.dialog.open(DialogDoctorComponent, {
      data: { type: 'create' }
    });
    // dialogRef.updateSize("400px","500px");
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      this.loadDataTable();
    });
  }

  editRecord(doctor: IDoctor) {
    let dialogRef = this.dialog.open(DialogDoctorComponent, {
      data: { type: 'edit', id: doctor.id, name: doctor.name, gender: doctor.gender, 
              title: doctor.title, specialist: doctor.specialist, unit: doctor.unit }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      this.loadDataTable();
    },
    error => console.log('Update Fail', error)
    );
  }

  deleteRecord(doctor: IDoctor): void {
    console.log('aaaaaaaaaa', doctor.id);

    this._doctorService.deleteDoctor(doctor.id)
      .subscribe(
        response => {
          this.loadDataTable();
          console.log('Success!', response)
        },
        error => console.error('Delete Fail!', error)
      );
    
  };
}
