import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { FormBuilder } from '@angular/forms';
import { IDoctor } from '../Entity/doctor';
@Injectable({
  providedIn: 'root'
})
export class DoctorService {
  constructor(private _http: HttpClient, private fb: FormBuilder) { }
  // _url = '../assets/fixedData/record.json';
  //_url = 'http://localhost:3001/entity';
  _url = 'http://localhost:3001/doctor';

  getAllDoctors(): Observable<IDoctor[]>{
    return this._http.get<IDoctor[]>(this._url)
                    .pipe(catchError(this.errorHandler));
  }
  
  getDoctorById(id: number): Observable<IDoctor[]>{
    return this._http.get<IDoctor[]>(this._url  + '/' + id)
                    .pipe(catchError(this.errorHandler));
  }

  addDoctor(userData){
    return this._http.post<any>(this._url, userData)
                     .pipe(catchError(this.errorHandler));
  }

  deleteDoctor(id: number){
    return this._http.delete<any>(this._url + '/' + id);
  }

  errorHandler(error: HttpErrorResponse){
    return Observable.throw(error.message || "Server Error");
  }

  updateDoctor(id,userData){
    return this._http.put<any>(this._url + '/' + id, userData);
  }

}
