import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, Validators } from '@angular/forms';
import { forbiddenNameValidator } from '../shared/name.validator';
import { DoctorService } from '../services/doctor.service';




@Component({
  selector: 'app-dialog-doctor',
  templateUrl: './dialog-doctor.component.html',
  styleUrls: ['./dialog-doctor.component.css']
})
export class DialogDoctorComponent implements OnInit {
  // get form control name
  get name() {
    return this.addForm.get('name');
  }
  // get form control gender
  get gender(){
    return this.addForm.get('gender');
  }
  // get title form control
  get title(){
    return this.addForm.get('title');
  }
  // get specialist form control
  get specialist(){
    return this.addForm.get('specialist');
  }
  // get unit form control
  get unit(){
    return this.addForm.get('unit');
  }
  
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, 
              private fb: FormBuilder, 
              private _doctorService: DoctorService) { }

  ngOnInit() {
  }
  
  addForm = this.fb.group({
    name: ['', [Validators.required, Validators.minLength(3), forbiddenNameValidator(/admin/)]],
    gender: ['',Validators.required],
    title: ['',Validators.required],
    specialist: ['', Validators.required],
    unit: ['', Validators.required]
  });


  // Function: submit data to server

  onSubmit(id) {  
      if(id === undefined){
        this._doctorService.addDoctor(this.addForm.value)
        .subscribe(
          response => console.log('Success!', response),
          error => console.error('Error!',error)
        ); 
       
      }else{
        console.log('update',id);
        this._doctorService.updateDoctor(id,this.addForm.value)
        .subscribe(
          response => console.log('Success!', response),
          error => console.error('Error!',error)
        ); 
        
      }

  }
}
