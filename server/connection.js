const mysql = require('mysql');
const fs = require('fs');

const pool = mysql.createPool({
    connectionLimit : 10,
    host     : 'mycapstone.mysql.database.azure.com',
    user     : 'myadmin@mycapstone',
    password : 'AzureUser@@12',
    database : 'blouse', 
    port     : 3306,
    ssl      : {
        ca   : fs.readFileSync('./ssl/BaltimoreCyberTrustRoot.crt.pem')
    }
});

// pool.connect(function(err) {
//     if (err) {
//       console.error('error connecting: ' + err.message);
//       return;
//     }
  
//     console.log('connected as id ' + connection.threadId);
// });

module.exports = pool;