const express = require('express');
const pool = require('./connection');
const entityRouter = require('./routers/entity');
const doctorRouter = require('./routers/doctor');
const departmentRouter = require('./routers/department');
const userRouter = require('./routers/user');

const app = express();

app.use(function (req, res, next) {
  /*var err = new Error('Not Found');
   err.status = 404;
   next(err);*/

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization');

//  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  // Pass to next layer of middleware
  next();
});

app.get('/', (req, res) => {
    res.send('Hello from the other side!!');
});
app.use('/entity', entityRouter);
app.use('/doctor', doctorRouter);
app.use('/department', departmentRouter);
app.use('/user', userRouter);

app.listen(3001, () => {
    console.log('Server is listening at http://localhost:3001');
});