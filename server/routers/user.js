const express = require("express");
const pool = require("../connection");
const bodyParser = require("body-parser");
// const cors = require('./cors');
// .options(cors.corsWithOptions, (req, res, next) => {
//     res.sendStatus(200);
// })
//SELECT * FROM blouse.user

const router = express.Router();

router.use(bodyParser.json());

router
  .route("/")
  .get((req, res, next) => {
    pool.query("CALL `blouse`.`search_doctor`(" +  + ", NULL, 'Gia', 1)", (err, results) => {
      if (err) console.error(err);
      res.send(results);
    });
  })
  .post((req, res, next) => {
    pool.query(
      "INSERT INTO blouse.user" +
        "(`username`, `password`, `isActive`) " +
        "VALUES ('" +
        req.body.username +
        "', '" +
        req.body.password +
        "', " +
        req.body.isActive +
        ")",
      (err, result) => {
        if (err) console.error(err);
        console.table(result);
        res.send(result);
      }
    );
  });

router
  .route("/:userId")
  .get((req, res, next) => {
    pool.query(
      "SELECT * FROM blouse.user WHERE id = " + req.params.userId,
      (err, result) => {
        if (err) console.error(err);
        console.table(result);
        res.send(result);
      }
    );
  })
  .put((req, res, next) => {
    pool.query(
      "UPDATE blouse.user SET " +
      "`username` = '" +
      req.body.username +
      "', " +
      "`password` = '" +
      req.body.password +
      "', " +
      "`isActive` = " +
      req.body.isActive + 
      " WHERE `id` = " +
      req.params.userId,
      (err, result) => {
        if (err) console.error(err);
        console.table(result);
        res.send(result);
      }
    );
  })
  .delete((req, res, next) => {
    pool.query(
      "DELETE FROM blouse.user WHERE id = " + req.params.userId,
      (err, result) => {
        if (err) console.error(err);
        console.table(result);
        res.send(result);
      }
    );
  });

module.exports = router;
