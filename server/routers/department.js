const express = require('express');
const pool = require('../connection');
const bodyParser = require('body-parser');
// const cors = require('./cors');
// .options(cors.corsWithOptions, (req, res, next) => {
//     res.sendStatus(200);
// })

const router = express.Router();

router.use(bodyParser.json());

router.route('/')
.get((req, res, next) => {
  pool.query('SELECT * FROM `blouse`.`department`', (err, results) => {
    if (err) console.error(err);
    res.send(results);
  });
})
.post((req, res, next) => {
  console.log('INSERT INTO `blouse`.`department` ' +
    '(`name`, `description`, `type`) ' +
    'VALUES (\'' +
    req.body.name + '\', \'' +
    req.body.description + '\', \'' +
    req.body.type +
    '\')');

  pool.query('INSERT INTO `blouse`.`department` ' +
  '(`name`, `description`, `type`) ' +
  'VALUES (\'' +
  req.body.name + '\', \'' +
  req.body.description + '\', \'' +
  req.body.type +
  '\')', (err, result) => {
    if (err) console.error(err);
    console.table(result);
    res.send(result);
  });
});

router.route('/:departmentId')
.get((req, res, next) => {
  pool.query('SELECT * FROM `blouse`.`department` WHERE id = ' + req.params.departmentId, (err, result) => {
    if (err) console.error(err);
    console.table(result);
    res.send(result);
  });
})
.put((req, res, next) => {
  console.log('UPDATE `blouse`.`department` SET ' + 
  '`name` = \'' + req.body.name + '\', ' + 
  '`description` = \'' + req.body.description + '\', ' + 
  '`type` = \'' + req.body.type + '\' ' + 
  'WHERE `id` = ' + req.params.departmentId);

  pool.query('UPDATE `blouse`.`department` SET ' + 
  '`name` = \'' + req.body.name + '\', ' + 
  '`description` = \'' + req.body.description + '\', ' + 
  '`type` = \'' + req.body.type + '\' ' + 
  'WHERE `id` = ' + req.params.departmentId, (err, result) => {
    if (err) console.error(err);
    console.table(result);
    res.send(result);
  });
})
.delete((req, res, next) => {
  pool.query('DELETE FROM `blouse`.`department` WHERE id = ' + req.params.departmentId, (err, result) => {
    if (err) console.error(err);
    console.table(result);
    res.send(result);
  });
});

module.exports = router;