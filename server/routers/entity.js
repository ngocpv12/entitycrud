const express = require('express');
const pool = require('../connection');
const bodyParser = require('body-parser');
// const cors = require('./cors');
// .options(cors.corsWithOptions, (req, res, next) => {
//     res.sendStatus(200);
// })

const router = express.Router();

router.use(bodyParser.json());

router.route('/')
.get((req, res, next) => {
  pool.query('SELECT * FROM demo_entity', (err, results) => {
    if (err) console.error(err);
    res.send(results);
  });
  // pool.getConnection((err, connection) => {
  //   if (err) console.error(err);
  //   connection.query('SELECT * FROM demo_entity', (err, results) => {
  //     res.send(results);
      
  //     connection.release();
  //     if (err) console.error(err);
  //   })
  // });
})
.post((req, res, next) => {
  pool.query('INSERT INTO demo_entity(name) VALUES (\'' + req.body.name + '\')', (err, result) => {
    if (err) console.error(err);
    console.table(result);
    res.send(result);
  });
});

router.route('/:entityId')
.get((req, res, next) => {
  pool.query('SELECT * FROM demo_entity WHERE id = ' + req.params.entityId, (err, result) => {
    if (err) console.error(err);
    console.table(result);
    res.send(result);
  });
})
.put((req, res, next) => {
  console.log('query', 'UPDATE demo_entity SET name = \'' + req.body.name + '\' WHERE id = ' + req.params.entityId);
  
  pool.query('UPDATE demo_entity SET name = \'' + req.body.name + '\' WHERE id = ' + req.params.entityId, (err, result) => {
    if (err) console.error(err);
    console.table(result);
    res.send(result);
  });
})
.delete((req, res, next) => {
  pool.query('DELETE FROM demo_entity WHERE id = ' + req.params.entityId, (err, result) => {
    if (err) console.error(err);
    console.table(result);
    res.send(result);
  });
});

module.exports = router;