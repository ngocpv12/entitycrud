const express = require("express");
const pool = require("../connection");
const bodyParser = require("body-parser");
// const cors = require('./cors');
// .options(cors.corsWithOptions, (req, res, next) => {
//     res.sendStatus(200);
// })

const router = express.Router();

router.use(bodyParser.json());

router
  .route("/")
  .get((req, res, next) => {
    console.log("h - hospitalId: " + req.query.h + ", d - departmentId: " + req.query.d + ", n - doctorName: " + req.query.n);
    console.log("CALL `blouse`.`search_doctor`(" +
    (req.query.h || null) +
    ", " +
    (req.query.d || null) +
    ", '" +
    (req.query.n || '') +
    "', 1)");    

    pool.query(
      "CALL `blouse`.`search_doctor`(" +
        (req.query.h || null) +
        ", " +
        (req.query.d || null) +
        ", '" +
        (req.query.n || '') +
        "', 1)",
      (err, results) => {
        if (err) console.error(err);
        res.send(results[0]);
      }
    );
  })
  .post((req, res, next) => {
    console.log(
      "INSERT INTO `blouse`.`doctor` " +
        "(`name`, `gender`, `title`, `specialist`, `unit`) " +
        "VALUES ('" +
        req.body.name +
        "', " +
        req.body.gender +
        ", '" +
        req.body.title +
        "', '" +
        req.body.specialist +
        "', '" +
        req.body.unit +
        "')"
    );

    pool.query(
      "INSERT INTO `blouse`.`doctor` " +
        "(`name`, `gender`, `title`, `specialist`, `unit`) " +
        "VALUES ('" +
        req.body.name +
        "', " +
        req.body.gender +
        ", '" +
        req.body.title +
        "', '" +
        req.body.specialist +
        "', '" +
        req.body.unit +
        "')",
      (err, result) => {
        if (err) console.error(err);
        console.table(result);
        res.send(result);
      }
    );
  });

router
  .route("/:doctorId")
  .get((req, res, next) => {
    pool.query(
      "SELECT * FROM `blouse`.`doctor` WHERE id = " + req.params.doctorId,
      (err, result) => {
        if (err) console.error(err);
        console.table(result);
        res.send(result);
      }
    );
  })
  .put((req, res, next) => {
    console.log(
      "UPDATE `blouse`.`doctor` SET " +
        "`name` = '" +
        req.body.name +
        "', " +
        "`gender` = " +
        req.body.gender +
        ", " +
        "`title` = '" +
        req.body.title +
        "', " +
        "`specialist` = '" +
        req.body.specialist +
        "', " +
        "`unit` = '" +
        req.body.unit +
        "' " +
        "WHERE `id` = " +
        req.params.doctorId
    );

    pool.query(
      "UPDATE `blouse`.`doctor` SET " +
        "`name` = '" +
        req.body.name +
        "', " +
        "`gender` = " +
        req.body.gender +
        ", " +
        "`title` = '" +
        req.body.title +
        "', " +
        "`specialist` = '" +
        req.body.specialist +
        "', " +
        "`unit` = '" +
        req.body.unit +
        "' " +
        "WHERE `id` = " +
        req.params.doctorId,
      (err, result) => {
        if (err) console.error(err);
        console.table(result);
        res.send(result);
      }
    );
  })
  .delete((req, res, next) => {
    pool.query(
      "DELETE FROM `blouse`.`doctor` WHERE id = " + req.params.doctorId,
      (err, result) => {
        if (err) console.error(err);
        console.table(result);
        res.send(result);
      }
    );
  });

// router
//   .route('/:hospitalId&:departmentId&:doctorName')
//   .get((req, res, next) => {
//     console.log(req.params);

//     pool.query("CALL `blouse`.`search_doctor`(" + req.params.hospitalId + ", " + req.params.departmentId + ", '" + req.params.doctorName + "', 1)", (err, results) => {
//       if (err) console.error(err);
//       res.send(results);
//     });
//   })

module.exports = router;
